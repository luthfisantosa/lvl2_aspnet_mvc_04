﻿using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPnet_MVC_04
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
