namespace LVL2_ASPnet_MVC_04.Areas.HelpPage.ModelDescriptions
{
    public class EnumValueDescription
    {
        public string Documentation { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}